Pod::Spec.new do |s|

  s.name             = 'JDCAnnotationClusteringSwift'
  s.version          = '1.0.0'
  s.summary          = 'A fork of FBAnnotationClusteringSwift with Swift 5 compatibility.'
  s.description      = <<-DESC
A fork of FBAnnotationClusteringSwift with Swift 5 compatibility for clustering on maps.
                       DESC

  s.homepage         = 'https://bitbucket.org/neospirit/jdcannotationclusteringswift.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'neospirit' => 'demare.cyrille@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/neospirit/jdcannotationclusteringswift.git', :tag => s.version.to_s }
  s.swift_versions   = '5.0'
  
  s.ios.deployment_target = '10.0'

  s.source_files = 'JDCAnnotationClusteringSwift/Classes/**/*'
  s.resource_bundles = {
    'JDCAnnotationClusteringSwift' => ['JDCAnnotationClusteringSwift/Assets/*.*']
  }

end
