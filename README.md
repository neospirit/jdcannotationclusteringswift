# JDCAnnotationClusteringSwift

[![CI Status](https://img.shields.io/travis/neospirit/JDCAnnotationClusteringSwift.svg?style=flat)](https://travis-ci.org/neospirit/JDCAnnotationClusteringSwift)
[![Version](https://img.shields.io/cocoapods/v/JDCAnnotationClusteringSwift.svg?style=flat)](https://cocoapods.org/pods/JDCAnnotationClusteringSwift)
[![License](https://img.shields.io/cocoapods/l/JDCAnnotationClusteringSwift.svg?style=flat)](https://cocoapods.org/pods/JDCAnnotationClusteringSwift)
[![Platform](https://img.shields.io/cocoapods/p/JDCAnnotationClusteringSwift.svg?style=flat)](https://cocoapods.org/pods/JDCAnnotationClusteringSwift)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JDCAnnotationClusteringSwift is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JDCAnnotationClusteringSwift'
```

## Author

demare.cyrille@gmail.com

## License

JDCAnnotationClusteringSwift is available under the MIT license. See the LICENSE file for more info.
